# :snowflake: LVM

## 1. PV

- **パーティション作成**

      fdisk /dev/sdX

  ※パーティションのパーティションタイプはLVMに変更する  

      Command (m for help): p

      Disk /dev/sdb: 10.7 GB, 10737418240 bytes, 20971520 sectors
      Units = sectors of 1 * 512 = 512 bytes
      Sector size (logical/physical): 512 bytes / 512 bytes
      I/O size (minimum/optimal): 512 bytes / 512 bytes
      Disk label type: dos
      Disk identifier: 0x9a40bfff

       Device Boot      Start         End      Blocks   Id      System
      /dev/sdb1            2048    20971519    10484736   8e      Linux LVM

      Command (m for help):

- **PV作成**

      pvcreate <disk-name>

- **PV確認**

      pvdisplay <pv-name>

- **【 lvmdiskscan 】コマンド**  
・・・物理ボリュームとして使用可能なデバイスを表示する

- **【 pvscan 】コマンド**  
・・・ デバイスをスキャンして物理ボリュームの情報を一覧表示する

- **【 pvchange 】コマンド**  
・・・物理ボリューム上でのPEPE割り当て防止

      pvchange -x n /dev/sdX

  ※割り当てをもとに戻す場合

      pvchange -x y /dev/sdX

- **【 pvresize 】コマンド**  
・・・物理ボリュームのサイズを変更する

- **【 pvremove 】コマンド**  
・・・物理ボリュームを削除する

## 2. VG

- **VG作成**

      vgcreate <vg-name> <disk-name1> <disk-name2> ...

- **確認**

      vgdisplay <vg-name>

  ※「**-v**」をつけると詳細表示できる。

- **【 vgextend 】コマンド**  
・・・ボリュームグループに物理ボリュームを追加する

      vgexpand <vg-name> <disk-name>

- **【 vgreduce 】コマンド**
・・・ボリュームグループから物理ボリュームを削除する

      vgreduce <vg-name> <disk-name>      

- **【 vgscan 】コマンド**  
・・・デバイスをスキャンしてボリュームグループを一覧表示する

- **【 vgremove 】コマンド**  
・・・ボリュームグループを削除する

- **【 vgchange 】コマンド**  
・・・ボリュームグループを非アクティブ化し、カーネルに認識されないようにする。  
*※その他にもVGに関する様々なパラメータの変更が可能。*

      vgchange -x n <vg-name>

  ※VGを有効化する

      vgchange -x y <vg-name>

- **VGの分割**  
・・・既にLVにデバイスが割り当てられている状態でVGをどのように分割するか。

  - LVに割り当てられているPVの確認
  
        lvs vglab -o +devices
          LV     VG    Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert Devices
          lvol01 vglab -wi-a----- 5.00g                                                     /dev/sdb1(0)
          lvol02 vglab -wi-a----- 5.00g                                                     /dev/sdc1(0)

  ※上記の場合はlvol02というLVに/dev/sdc1というデバイスが割り当てられている。  
  　これを別のVGに分割する。

  - 対象のLVを無効化

        lvchange -a n <lv-name>

  - デバイスをVGから分割

        vgsplit <current-vg-name> <new-vg-name> <device-name>

  - 確認

        lvs vglab -o +devices
          LV     VG    Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert Devices
          lvol01 vglab -wi-a----- 5.00g                                                     /dev/sdb1(0)

        lvs newvglab -o +devices
          LV     VG       Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert Devices
          lvol02 newvglab -wi------- 5.00g                                                     /dev/sdc1(0)

  - 無効化したVGの有効化

        vgchange -a y <new-vg-name>

- **VGのマージ**  
※はじめにマージされる側のVGは無効化しておく。

      vgmerge -v <original-vg-name> <merged-vg-name>

- **VGメタデータのバックアップ**  

  - 「**/etc/lvm/backup**」・・・最新のバックアップデータを格納
  - 「**/etc/lvm/archive**」・・・すべてのデータを格納

  - 手動でのバックアップ
  
        vgcfgbackup <vg-name>

  - バックアップリストア

        vgcfgrestore <backup-vg-name>

## 3. LV

・**LV作成**

- 全体サイズ指定
  
      lvcreate -L <size-of-lv> -n <lv-name> <vg-name>

- 作成する論理ボリュームのサイズを論理エクステント（※2）の個数、または割合（「%VG」「%PVS」「%FREE」「%ORIGIN」）で指定する

      lvcreate -l <size-of-lv> -n <lv-name> <vg-name> <disk-name1>:<min>-<max> ...

- **LV確認**

      lvdisplay <lv-name>

- **LV削除**

      lvremove <lv-name>

- **Stripd LV**

      lvcreate -L <size-of-lv> -i<number-of-disk> -I<size-of-chunk> -n <lv-name <vg-name>

- **LVの無効化・有効化**

      lvchange -a <n/y> <lv-name>

- **LVMミラーリング**

      lvcreate --type mirror -L <size-of-lv> -m <number-of-copy> -n <lv-name> <vg-name>

- **LVの拡張**

      lvextend -L +<increase-size> <lv-name>
  
  ※既にマウントされている場合はファイルシステム自体の拡張も必要  

      resize2fs <file-system-name>

## 4. Thin Provisioning LV

- メリット  
・・・利用可能なエクステントより大きい論理ボリュームを作成することができる。

      lvcreate -L <size>G --thinpool <pool-name> <vg-name>

      lvcreate -V <size>G --thin -n <thin_volume_name> <vg-name>/<pool_name>

## 5. LVM Snapshot

- スナップショットの作成

      lvcreate -L <size> -s -n <snapshot-name> <file-system-name>

  ※\<size>は実際の使用しているファイルサイズを指定する。

- スナップショットを使用したロールバック

      lvconvert --merge <snap-vol-path>
